const express = require('express');
const router = express.Router();
const apiDbMongo = require('../database');
const {isLoggedIn} = require('../lib/auth');

router.get('/add',isLoggedIn, (req,res) => {
  res.render('accounts/add.hbs');
});

router.post('/add', isLoggedIn, async (req, res) => {
  const {iban,balance} = req.body;
  const clientMLab = apiDbMongo.clientReqJson;
  const apiKey = apiDbMongo.apiKeyMLab;
  await clientMLab.get('accounts?' + apiKey, async (errGet, resGet, bodyGet) => {
    var accountId = bodyGet.length + 1;
    var userId = req.user.userId;
    const newAccount = {
      accountId,
      userId,
      iban,
      balance
    };
    await clientMLab.post('accounts?' + apiKey, newAccount, (errPost, resPost, bodyPost) => {
      req.flash('success','Account saved successfully');
      res.redirect('/accounts');
    });
  });
});

router.get('/', isLoggedIn, async (req, res) => {
  const clientMLab = apiDbMongo.clientReqJson;
  const apiKey = apiDbMongo.apiKeyMLab;
  const  queryString='f={"_id":0}&';
  const queryStringId = 'q={"userId":' + req.user.userId + '}&';
  await clientMLab.get('accounts?' + queryString + queryStringId + apiKey, (errGet, resGet, bodyGet) => {
      const accounts = bodyGet;
      res.render('accounts/list',{accounts});
  });
});

router.get('/delete/:id', isLoggedIn, async (req, res) => {
  const accountId = req.params.id;
  const clientMLab = apiDbMongo.clientReqJson;
  const apiKey = apiDbMongo.apiKeyMLab;
  const queryStringId = 'q={"accountId":' + accountId + '}&';
  await clientMLab.get('accounts?' + queryStringId + apiKey, async (errGet, resGet, bodyGet) => {
      const account = bodyGet[0];
       await clientMLab.delete('accounts/' + account._id.$oid + '?' + apiKey, (errDel, resDel, bodyDel) => {
        req.flash('success','Account removed successfully');
        res.redirect('/accounts');
      });
  });
});

router.get('/edit/:id', isLoggedIn, async (req, res) => {
  const accountId = req.params.id;
  const clientMLab = apiDbMongo.clientReqJson;
  const apiKey = apiDbMongo.apiKeyMLab;
  const queryStringId = 'q={"accountId":' + accountId + '}&';
  await clientMLab.get('accounts?' + queryStringId + apiKey, (errGet, resGet, bodyGet) => {
      const account = bodyGet[0];
      res.render('accounts/edit',account);
  });
});

router.post('/edit/:id', isLoggedIn, async (req, res) => {
  const accountId = req.params.id;
  const cambio = '{"$set":' + JSON.stringify(req.body) + '}';
  const clientMLab = apiDbMongo.clientReqJson;
  const apiKey = apiDbMongo.apiKeyMLab;
  const queryStringId = 'q={"accountId":' + accountId + '}&';
  await clientMLab.get('accounts?' + queryStringId + apiKey, async (errGet, resGet, bodyGet) => {
    const account = bodyGet[0];
    await clientMLab.put('accounts/' + account._id.$oid + '?' + apiKey, JSON.parse(cambio), (errPut, resPut, bodyPut) => {
      req.flash('success','Account updated successfully');
      res.redirect('/accounts');
    });
 });
});

module.exports = router;
