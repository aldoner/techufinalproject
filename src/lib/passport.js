const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const apiDbMongo = require('../database');
const helpers = require('../lib/helpers');

passport.use('local.signin', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true
}, async (req, username, password, done) => {
    const clientMLab = apiDbMongo.clientReqJson;
    const apiKey = apiDbMongo.apiKeyMLab;
    const queryString='f={"_id":0}&';
    const queryStringID='q={"username":"' + username + '"}&';
    clientMLab.get('users?' + queryString + queryStringID + apiKey, async (errGet, resGet, bodyGet) => {      
      if (bodyGet.length > 0) {
        const savedUser = bodyGet[0];
        const validPassword = await helpers.matchPassword(password,savedUser.password);
        if (validPassword) {
            done(null,savedUser,req.flash('success','Welcome ' + savedUser.username));
        }else {
          done(null,false,req.flash('message','Incorrect Password'));
        }
      }else {
        done(null,false,req.flash('message','Username does not exists'));
      }
    });
}));

passport.use('local.signup', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true
}, async (req, username, password, done) => {
    const {fullname} = req.body;
    const clientMLab = apiDbMongo.clientReqJson;
    const apiKey = apiDbMongo.apiKeyMLab;
    clientMLab.get('users?' + apiKey, async (errGet, resGet, bodyGet) => {
      var userId = bodyGet.length + 1;
      const newUser = {
        userId,
        username,
        password,
        fullname
      };
      newUser.password = await helpers.encryptPassword(password);
      clientMLab.post('users?' + apiKey, newUser, async (errPost, resPost, bodyPost) => {
        return done(null,newUser);
      });
    });
}));

passport.serializeUser((user, done) => {
  done(null, user.userId);
});

passport.deserializeUser(async (id, done) => {
  const clientMLab = apiDbMongo.clientReqJson;
  const apiKey = apiDbMongo.apiKeyMLab;
  const queryString='f={"_id":0}&';
  const queryStringID='q={"userId":' + id + '}&';
  clientMLab.get('users?' + queryString + queryStringID + apiKey, async (errGet, resGet, bodyGet) => {
    done(null, bodyGet[0]);
  });
});
