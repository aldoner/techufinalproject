const requestJson = require('request-json');
const {database} = require('./keys.js');
//const promisify = require('util');
const baseMLabUrl = database.host;
const apiKeyMLab = database.password;
const clientReqJson = requestJson.createClient(baseMLabUrl);

//Promisify Pool Querys
//pool.query = promisify(pool.query);
exports.clientReqJson = clientReqJson;
exports.apiKeyMLab =  apiKeyMLab;
exports.baseMLabUrl = baseMLabUrl;
